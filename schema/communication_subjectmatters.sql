DROP TABLE IF EXISTS communication_subjectmatters CASCADE;

CREATE TABLE communication_subjectmatters (
	COMLOG_ID	bigint
	, CONCERN_TYPE	text
	, CONCERN_NAME	text
);

ALTER TABLE communication_subjectmatters OWNER TO lobbyist;

CREATE INDEX communication_subjectmatters_COMLOG_ID_idx ON communication_subjectmatters (COMLOG_ID);
