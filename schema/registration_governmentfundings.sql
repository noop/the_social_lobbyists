DROP TABLE IF EXISTS registration_govtfundings;

CREATE TABLE registration_govtfundings (
    REGISTRATION_ID		    bigserial PRIMARY KEY
    , INSTITUTION		    text
    , AMOUNT			    text
    , FUNDS_EXPECTED_CURRENT_YEAR   text
    , "TEXT"			    text
);

ALTER TABLE registration_govtfundings OWNER TO lobbyist;
