DROP TABLE IF EXISTS communication_primary;

CREATE TABLE communication_primary (
    COMLOG_ID		bigint
    , COMLOG_VERSION	smallint
    , CLIENT_NUMBER	bigint
    , REGISTRANT_NUMBER	bigint
    , MEETING_DATE	timestamp
    , REGISTRATION_TYPE	smallint
    , POSTED_DATE	timestamp
);

ALTER TABLE communication_primary OWNER TO lobbyist;

CREATE INDEX communication_primary_COMLONG_ID_idx ON communication_primary (COMLOG_ID);

COMMENT ON TABLE communication_primary IS 'The main log from which to find communication between lobbyists and MPs.';
COMMENT ON COLUMN communication_primary.REGISTRATION_TYPE   IS 'Types: 1 = Consultant registration, 2 = Corporation registration, 3 = ORganization registration';
COMMENT ON COLUMN communication_primary.REGISTRANT_NUMBER   IS 'Links to registrants_primary.registrant_number';
COMMENT ON COLUMN communication_primary.CLIENT_NUMBER	    IS 'Links to registration_primary.client_number';
