DROP TABLE IF EXISTS registration_govtfunding;

CREATE TABLE registration_govtfunding (
    REGISTRATION_ID		    bigint
    , INSTITUTION		    text
    , AMOUNT			    text
    , FUNDS_EXPECTED_CURRENT_YEAR   text
    , "TEXT"			    text
);

ALTER TABLE registration_govtfunding OWNER TO lobbyist;

CREATE INDEX registration_govtfunding_REGISTRATION_ID_idx ON registration_govtfunding (REGISTRATION_ID);
