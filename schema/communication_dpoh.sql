DROP TABLE IF EXISTS communication_dpoh;

CREATE TABLE communication_dpoh (
    COMLOG_ID		bigint
    , DPOH_LAST_NAME	text
    , DPOH_FIRST_NAME	text
    , DPOH_TITLE	text
    , BRANCH_UNIT	text
    , INSTITUTION_NAME	text
    , INSTITUTION_TYPE	text
);

ALTER TABLE communication_dpoh OWNER TO lobbyist;

CREATE INDEX communication_dpoh_COMLOG_ID_idx ON communication_dpoh (COMLOG_ID);
CREATE INDEX communication_dpoh_DPOH_LAST_NAME_idx ON communication_dpoh (DPOH_LAST_NAME);
CREATE INDEX communication_dpoh_DPOH_TITLE_idx ON communication_dpoh (DPOH_TITLE);
