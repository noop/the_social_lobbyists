DROP TABLE IF EXISTS registration_subjectmatters;

CREATE TABLE registration_subjectmatters (
	REGISTRATION_ID	bigint 
	, CONCERN	text
);

ALTER TABLE registration_subjectmatters OWNER TO lobbyist;

CREATE INDEX registration_subjectmatters_REGISTRATION_ID_idx ON registration_subjectmatters (REGISTRATION_ID);
