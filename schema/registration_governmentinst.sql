DROP TABLE IF EXISTS registration_governmentinst;

CREATE TABLE registration_governmentinst (
    REGISTRATION_ID bigint
    , INSTITUTION   text
);

ALTER TABLE registration_governmentinst OWNER TO lobbyist;

CREATE INDEX registration_governmentinst_REGISTRATION_ID_idx ON registration_governmentinst (REGISTRATION_ID);
CREATE INDEX registration_governmentinst_INSTITUTION_idx ON registration_governmentinst (INSTITUTION);
