CREATE VIEW registration_registrant_info AS (
    SELECT DISTINCT
        registration_type
        , registrant_number
        , client_number
        , initcap(registrant_last_name)	    AS registrant_last_name
        , initcap(registrant_first_name)    AS registrant_first_name
    FROM registration_primary
);
