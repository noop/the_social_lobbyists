CREATE VIEW communication_concerns AS (
    SELECT
	comlog_id
	, array_agg ( DISTINCT concern_name ) AS concern_types
    FROM communication_subjectmatters
    GROUP BY comlog_id
);
