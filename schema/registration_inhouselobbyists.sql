DROP TABLE IF EXISTS registration_inhouselobbyists;

CREATE TABLE registration_inhouselobbyists (
    CLIENT_PROFILE_ID			bigint
    , LOBBYIST_ID			bigint
    , JOB_TITLE_NAME			text
    , LOBBY_LAST_NAME			text
    , LOBBY_FIRST_NAME			text
    , DPOH_INDICATOR			text
    , POH_INDICATOR			text
    , ADM_POSITION_LAST_HELD_DATE	timestamp
    , DPOH_POSITION_LAST_HELD_DATE	timestamp
    , TRANSITION_TEAM_END_DATE		timestamp
);

ALTER TABLE registration_inhouselobbyists OWNER TO lobbyist;

CREATE INDEX registration_inhouselobbyists_CLIENT_PROFILE_ID_idx ON registration_inhouselobbyists (CLIENT_PROFILE_ID);
