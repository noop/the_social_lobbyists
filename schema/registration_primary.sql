DROP TABLE IF EXISTS registration_primary CASCADE;

CREATE TABLE registration_primary (
    REGISTRATION_ID		    bigserial PRIMARY KEY
    ,REGISTRATION_TYPE	    	    smallint
    ,VERSION		    	    text
    ,VERSION_CODE	    	    text
    ,FIRM_NM_EN		    	    text
    ,FIRM_NM_FR		    	    text
    ,REGISTRANT_POSITION_IN_FIRM    text
    ,FIRM_ADDRESS		    text
    ,FIRM_TEL		    	    text
    ,FIRM_FAX		    	    text
    ,REGISTRANT_NUMBER		    bigint
    ,REGISTRANT_LAST_NAME	    text
    ,REGISTRANT_FIRST_NAME	    text
    ,REGISTRANT_BUSINESS_ADDRESS    text
    ,REGISTRANT_BUS_PHONE	    text
    ,REGISTRANT_BUS_FAX		    text
    ,CLIENT_PROFILE_ID		    bigint
    ,CLIENT_NUMBER		    bigint
    ,CLIENT_NAME_EN		    text
    ,CLIENT_NAME_FR		    text
    ,CLIENT_ADDRESS		    text
    ,CLIENT_PHONE		    text
    ,CLIENT_FAX			    text
    ,PRINCIPAL_REP_LAST_NM	    text
    ,PRINCIPAL_REP_FIRST_NM	    text
    ,PRINCIPAL_REP_POSITION	    text
    ,EFFECTIVE_DATE		    timestamp
    ,END_DATE			    timestamp
    ,BUSINESS_REL_PARENT_IND	    text
    ,BUSINESS_REL_COALITION_IND	    text
    ,OTHR_BNFCRY_SUBSIDIARY_IND	    text
    ,OTHR_BNFCRY_DIRECT_INTRST_IND  text
    ,OCLRS_GOVT_FUND_IND	    boolean
    ,FYR_END_DT			    text
    ,CONTINGENCY_FEE_INDICATOR	    text
    ,PREVIOUS_REGISTRATION_ID	    bigint
    ,POSTED_DATE		    timestamp
);

ALTER TABLE registration_primary OWNER TO lobbyist;

COMMENT ON TABLE registration_primary IS 'A table hosting all of the registrations and to whom it is linked.';
COMMENT ON COLUMN registration_primary.REGISTRATION_ID	    IS 'The ID of the registration, unique to an event?';
COMMENT ON COLUMN registration_primary.REGISTRATION_TYPE    IS 'Types: 1,2,3. 3 indicated a link to registration_inhouselobbyists.';
COMMENT ON COLUMN registration_primary.CLIENT_PROFILE_ID    IS 'An ID that profiles a group or organization?';
