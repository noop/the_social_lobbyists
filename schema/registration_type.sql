DROP TABLE IF EXISTS registration_type CASCADE;

CREATE TABLE registration_type (
	id	smallint
	, name  text
);

ALTER TABLE registration_type OWNER TO lobbyist;

INSERT INTO registration_type (id, name) VALUES ( 1, 'Consultant registration' ), ( 2, 'Corporation registration' ), ( 3, 'Organization registration') ;
