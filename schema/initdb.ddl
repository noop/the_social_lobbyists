CREATE DATABASE the_social;

CREATE USER lobbyist PASSWORD 'blahblah';

ALTER DATABASE the_social OWNER TO lobbyist;
