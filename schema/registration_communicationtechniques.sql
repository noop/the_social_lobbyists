DROP TABLE IF EXISTS registration_communicationtechniques;

CREATE TABLE registration_communicationtechniques (
    REGISTRATION_ID		    bigint
    , COMMUNICATION_NAME	    text
    , COMMUNICATION_DESCRIPTION	    text
);

ALTER TABLE registration_communicationtechniques OWNER TO lobbyist;

CREATE INDEX registration_communicationtechniques_registration_id ON registration_communicationtechniques (REGISTRATION_ID);
