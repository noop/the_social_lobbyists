DROP TABLE IF EXISTS registration_consultantlobbyists;

CREATE TABLE registration_consultantlobbyists (
    CLIENT_PROFILE_ID			    bigint
    , LOBBYIST_ID			    bigint
    , JOB_TITLE_NAME			    text
    , LOBBY_LAST_NAME			    text
    , LOBBY_FIRST_NAME			    text
    , DPOH_INDICATOR			    text
    , POH_INDICATOR			    text
    , ADM_POSITION_LAST_HELD_DATE	    text
    , DPOH_POSITION_LAST_HELD_DATE	    text
    , TRANSITION_TEAM_END_DATE		    text
    , CORP_BOARD_MEMBER			    text
    , ORG_MEMBER_OR_ORG_BOARD_MEMBER	    text
    , ORG_MEMBER			    text 
    , ORG_BOARD_MEMBER			    text 
);

ALTER TABLE registration_consultantlobbyists OWNER TO lobbyist;

CREATE INDEX registration_consultantlobbyists_CLIENT_PROFILE_ID_idx ON registration_consultantlobbyists (CLIENT_PROFILE_ID);
