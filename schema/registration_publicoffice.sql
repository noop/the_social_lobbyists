DROP TABLE IF EXISTS registration_publicoffice;

CREATE TABLE registration_publicoffice (
    LOBBYIST_ID				bigint
    , DPOH_INDICATOR			text
    , TITLE				text
    , BRANCH				text
    , DEPT				text
    , POSITION_START_DATE		timestamp
    , POSITION_END_DATE			timestamp
    , ADM_EQUIVALENT			text
    , DPOH_LAST_DATE			timestamp
    , DPOH_EMPLOYMENT_XCHNG_PROGRAM	text
    , POH_DETAILS_PRE_V5		text
);

ALTER TABLE registration_publicoffice OWNER TO lobbyist;

CREATE INDEX registration_publicoffice_LOBBYIST_ID_idx ON registration_publicoffice (LOBBYIST_ID);
