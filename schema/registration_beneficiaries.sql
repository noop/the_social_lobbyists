DROP TABLE IF EXISTS registration_beneficiaries;

CREATE TABLE registration_beneficiaries (
    REGISTRATION_ID	    bigint
    , TYPE_CD		    smallint
    , BENEFICIARY_NAME_EN   text
    , BENEFICIARY_NAME_FR   text
    , STREET_1		    text
    , STREET_2		    text
    , CITY		    text
    , POST_CODE		    text
    , PROV_STATE	    text
    , COUNTRY		    text
);

ALTER TABLE registration_beneficiaries OWNER TO lobbyist;

CREATE INDEX registration_beneficiaries_REGISTRATION_ID_idx ON registration_beneficiaries (REGISTRATION_ID);
