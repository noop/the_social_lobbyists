DROP TABLE IF EXISTS registration_subjectmatterdetails;

CREATE TABLE registration_subjectmatterdetails (
	REGISTRATION_ID	bigint
	, SUBJECT	text
	, DESCRIPTION	text
);

ALTER TABLE registration_subjectmatterdetails OWNER TO lobbyist;

CREATE INDEX registration_subjectmatterdetails_REGISTRATION_ID_idx ON registration_subjectmatterdetails (REGISTRATION_ID);
