DROP TABLE IF EXISTS registration_governmentinsts;

CREATE TABLE registration_governmentinsts (
    REGISTRATION_ID bigint
    , INSTITUTION   text


);

ALTER TABLE registration_governmentinsts OWNER TO lobbyist;

CREATE INDEX registration_governmentinsts_REGISTRATION_ID_idx ON registration_governmentinsts (REGISTRATION_ID);
CREATE INDEX registration_governmentinsts_INSTITUTION_idx ON registration_governmentinsts (INSTITUTION);
