#!/bin/bash 
cd $(dirname $0);
## Imports the data files into the database defined below
source ../config/postgres_conf.sh ;

echo "DB HOST [ ${DB_HOST} ] DB PORT [ ${DB_PORT} ] DB_NAME [ ${DB_NAME} ] DB_USER [ ${DB_USER} ]";

TABLE_NAME=$(basename "${1}" | sed 's/Export.csv//');
if [ "${TABLE_NAME}" = "CommunicationSubjectMatters" ]; then
    TABLE_NAME="communication_subjectmatters";
fi
echo "Import file [ ${1} ] to Table [ ${TABLE_NAME} ]"; 
iconv --from-code=ISO-8859-1 --to-code=UTF-8 ${1} | psql -h ${DB_HOST} -p ${DB_PORT} -d ${DB_NAME} -U ${DB_USER} -c "\COPY ${TABLE_NAME} FROM STDIN WITH CSV HEADER";
