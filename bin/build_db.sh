#!/bin/bash
cd $(dirname $0);

## Build the database from scratch!

## Postgres stuff -- import from postgrec_conf.sh
source ../config/postgres_conf.sh

## Iterate through schema/
find ../schema/ -type f -name \*.sql | xargs -I{} bash -c "echo '{}'; psql -h ${DB_HOST} -p ${DB_PORT} -U ${DB_USER} -d ${DB_NAME} -f {}"

## Iterate through the data files
find ../data/* -type f -name \*.csv -exec bash -c "bash import.sh {}" \;
