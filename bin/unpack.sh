#!/bin/bash

## Make the data directory if it does not exist
if [ ! -d "../data" ]; then
    mkdir ../data
fi

## Unpacks the zip file into the data directory
unzip ${1} -d $(dirname $0)/../data/
