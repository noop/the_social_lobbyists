A project to import the Office of the Commissioner of Lobbying of Canada dataset into Postgres.  
  
This project will provide the ddls, unpacking programs and eventually graph generation tools.  
  
Data file URL: https://ocl-cal.gc.ca/eic/site/012.nsf/eng/00504.html  
  
Requirements:  
  
Postgres  
Bash   
find   
Pre-reqs:  
    A Postgres instance to deploy the DB to.  
    Configure config/postgres_conf.sh with your settings  
  
Init:  
    bash bin/unpack /path/to/LRS_extract-extrait_SEL.zip  
    bash bin/build_db.sh  

Tested on Centos/OS X / Postgres 9.2

FAQ  
Why are there Java bits included?  
The project will eventually include a Java unpacker and a toolkit to export social networks from the Lobbyist dataset. The intent is to do this in Java. The Bash system is a get-there-now solution.
