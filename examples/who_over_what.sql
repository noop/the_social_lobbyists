SELECT 
    meeting_date
    -- MP info
    , dpoh_title
    , dpoh_last_name
    -- Lobbyist info
    , rp.registrant_last_name
    , rp.registrant_first_name
    -- Meeting context
    , cs.concern_types
FROM communication_primary cp
JOIN communication_concerns cs
    ON cp.comlog_id = cs.comlog_id
JOIN communication_dpoh cd
    ON cp.comlog_id = cd.comlog_id
JOIN registration_registrant_info rp
    ON cp.registrant_number = rp.registrant_number 
	AND cp.client_number = rp.client_number
	AND cp.registration_type = rp.registration_type
ORDER BY cp.comlog_id
;
