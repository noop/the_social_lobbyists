-- Associates registration_id with legislation that the registrant lobbies on.
-- e.g.
--  registration_id | bill  
-------------------+-------
--          475703 | C-27
--          475797 | C-44
--          475811 | C-33
--          475844 | C-56
---------------------------
-- This can be used to inform future queries
-- THe optional two lines below create a view of this info

--CREATE VIEW registration_bills (registration_id, bill) 
--AS
SELECT registration_id, SUBSTRING(concern, 'C-[0-9]+') AS bill
FROM registration_subjectmatters 
WHERE concern ~ 'C-[0-9]+'
GROUP BY registration_id, bill
UNION
SELECT registration_id, SUBSTRING(description, 'C-[0-9]+') AS bill
FROM registration_subjectmatterdetails
WHERE description ~ 'C-[0-9]+'
GROUP BY registration_id, bill;


-- This query uses the view above to answer "Who Killed The Bill"
-- By searching for registrants who lobbied on a bill that died
-- on the flour of the House, we can see what might have 
-- contributed to its demise.
-- Unfortunately, a) Bill IDs are repeated from year-to-year, and
-- b) the subject matter table doesn't include any dates.  So we have
-- to approximate the date using when the registration was creating,
-- knowing that no registration created before the bill exists could
-- possibly be referring to the bill we care about.
-- In this query, we list the companies that lobbied on C-12, a bill
-- that tried to add breach notification to PIPEDA.

SELECT client_name_en, description 
FROM 
  (SELECT registration_id, client_name_en 
  FROM registration_primary 
  -- report only results from the last 2 years (C-12 was introduced in June 2011)
  WHERE effective_date > now()::date - interval '2y') rp
JOIN (SELECT registration_id FROM registration_bills WHERE bill='C-12') rb 
  ON rp.registration_id=rb.registration_id 
JOIN registration_subjectmatterdetails rd 
  ON rp.registration_id=rd.registration_id 
WHERE description ~ 'C-12' 
GROUP BY client_name_en, description;


-- This is the equivalent query, without requiring the above view to exist.
-- It has about the same run-time for common use cases.

SELECT client_name_en, description 
FROM 
  (SELECT registration_id, client_name_en 
  FROM registration_primary
  -- report only results from the last 2 years (C-12 was introduced in 2011)
  WHERE effective_date > now()::date - interval '2y') rp
JOIN 
  (SELECT registration_id, description 
  FROM registration_subjectmatterdetails 
  WHERE description~'C-12') rd 
  ON rp.registration_id=rd.registration_id 
GROUP BY client_name_en, description;



