SELECT 
    meeting_date
    , cp.comlog_id
    , cp.client_number
    , cp.registrant_number
    , cp.registration_type
    , cs.concern_types
    -- MP info
    , dpoh_last_name
    , dpoh_title
    -- Lobbyist info
    , rp.registrant_last_name
    , rp.registrant_first_name
FROM communication_primary cp
JOIN communication_concerns cs
    ON cp.comlog_id = cs.comlog_id
JOIN communication_dpoh cd
    ON cp.comlog_id = cd.comlog_id
JOIN registration_registrant_info rp
    ON cp.registrant_number = rp.registrant_number 
	AND cp.client_number = rp.client_number
	AND cp.registration_type = rp.registration_type
WHERE 1=1
    AND rp.registrant_number = 778519
ORDER BY cp.comlog_id
;
