SELECT 
    COUNT(rb.registration_id)
    , COALESCE(beneficiary_name_en, beneficiary_name_fr) benefactor_name
FROM registration_beneficiaries rb 
JOIN registration_primary rp 
ON rp.registration_id = rb.registration_id
GROUP BY COALESCE(beneficiary_name_en, beneficiary_name_fr)
ORDER BY 1 DESC
LIMIT 20
;

